#!/bin/bash

# Setting ROS WORKSPACE
echo "-Setting ROS workspace"
. $ACCELDYN_WORKSPACE/devel/setup.bash

# Setting ROSCORE
NETWORK_ROSCORE=$1
if [ -z $NETWORK_ROSCORE ] # Check if NETWORK_ROSCORE is NULL
  then
    echo "-Setting roscore in localhost"
    export ROS_MASTER_URI=http://localhost:11311
  else
    echo "-Setting roscore in $NETWORK_ROSCORE"
    export ROS_MASTER_URI=http://$NETWORK_ROSCORE:11311
fi
