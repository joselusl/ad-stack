## frontier_exploration

Info
http://wiki.ros.org/frontier_exploration


## explorer (single and multi-robot)

Info:
http://wiki.ros.org/explorer

Repo:
https://github.com/aau-ros/aau_multi_robot



## hector_navigation stack

Info:
http://wiki.ros.org/hector_navigation


# hector_exploration_planner
Info:
http://wiki.ros.org/hector_exploration_planner


# hector_exploration_node
Info:
http://wiki.ros.org/hector_exploration_node



## nav2d stack

Info:
http://wiki.ros.org/nav2d



## exploration stack (old)

Info:
http://wiki.ros.org/exploration



## explore_lite

Info:
http://wiki.ros.org/explore_lite

