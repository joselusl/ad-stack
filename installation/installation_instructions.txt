# Create a folder for the catkin workspace, for example:
mkdir -p $HOME/workspace/ros/accel_dynam_catkin_ws

# Create a folder to download repository, for example:
mkdir -p $HOME/workspace/ros/accel_dynam_catkin_ws/src/accel_dynam_stack

# Create an environmental variable with these paths:
export ACCELDYN_WORKSPACE=$HOME/workspace/ros/accel_dynam_catkin_ws/
export ACCELDYN_STACK=$HOME/workspace/ros/accel_dynam_catkin_ws/src/accel_dynam_stack

# Download repository:
cd $ACCELDYN_STACK
git clone https://bitbucket.org/joselusl/ad-stack 

# Init submodules
git submodule update --init

# Install all dependencies:
# TODO

# Compile with catkin
cd $ACCELDYN_WORKSPACE
source /opt/ros/indigo/setup.bash
catkin_make -DCMAKE_BUILD_TYPE=Release

